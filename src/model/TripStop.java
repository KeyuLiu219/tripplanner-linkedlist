package model;

/**
 *
 * @Description: model the trip stop
 * @author panbo
 *
 */
public class TripStop implements Cloneable{
	
	private String location;  
	
	private String activity;
	
	private Integer distance;

	
	/**
	 * 
	 */
	public TripStop() {
	
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException e) {
            throw new InternalError(e.toString());
        }
	}
	
	/**
	 * @param location
	 * @param activity
	 * @param distance
	 * @throws IllegalArgumentException
	 */
	public TripStop(String location, String activity, Integer distance) throws IllegalArgumentException{
		//check if distance > 0
		if(distance<0){
			throw new IllegalArgumentException("distance must greater than or equals to 0");
		}
		this.location = location;
		this.activity = activity;
		this.distance = distance;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the activity
	 */
	public String getActivity() {
		return activity;
	}

	/**
	 * @param activity the activity to set
	 */
	public void setActivity(String activity) {
		this.activity = activity;
	}

	/**
	 * @return the distance
	 */
	public Integer getDistance() {
		return distance;
	}

	/**
	 * @param distance the distance to set
	 * @throws IllegalArgumentException
	 */
	public void setDistance(Integer distance) throws IllegalArgumentException{
		if(distance<0){
			throw new IllegalArgumentException("This distance is invalid ‐‐ distances must be >= 0. Try again.");
		}
		this.distance = distance;
	}

	@Override
	public String toString() {
		return String.format("%-32s%-42s%d miles", location, activity, distance);
	}
	
	
}
