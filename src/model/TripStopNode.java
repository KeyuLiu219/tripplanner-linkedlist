/**   
 * @Title: TripStopNode.java 
 * @Package model 
 * @Description: TODO
 * @author pop
 * @date 2016年9月26日 下午9:23:27 
 * @version V1.0   
 */
package model;

/**
 *
 * @Description: Serves as a wrapper for the TripStop class
 * @author panbo
 *
 */
public class TripStopNode implements Cloneable{
	
	/**
	 * the data wrapped by node.It must not be null
	 */
	private TripStop data;
	
	/**
	 * next node 
	 */
	private TripStopNode next;
	
	/**
	 * prev node
	 */
	private TripStopNode prev;

	/**
	 * @param initData
	 * The data to be wrapped by this TripStopNode. This parameter should not
	 * be null, since we should never have a TripStopNode with null data
	 * @throws IllegalArgumentException
	 */
	public TripStopNode(TripStop initData) throws IllegalArgumentException{
		super();
		if(initData==null)
		{
			throw new IllegalArgumentException("initData must not be null");
		}
		this.data = initData;
		this.next=null;
		this.prev=null;
	}
	
	@Override
	public  Object clone() throws CloneNotSupportedException {
		TripStopNode node=null;
        try {
        	node=(TripStopNode)super.clone(); 
        }
        catch (CloneNotSupportedException e) {
            throw new InternalError(e.toString());
        }
        node.data=(TripStop)this.data.clone();
        return node;
	}

	/**
	 * @return the data
	 */
	public TripStop getData() {
		return data;
	}

	/**
	 * @param newData the data to set
	 */
	public void setData(TripStop newData) throws IllegalArgumentException{
		if(newData==null)
		{
			throw new IllegalArgumentException("initData must not be null");
		}
		this.data = newData;
	}

	/**
	 * @return the next node 
	 */
	public TripStopNode getNext() {
		return next;
	}

	/**
	 * @param next the next node to set
	 */
	public void setNext(TripStopNode next) {
		this.next = next;
	}

	/**
	 * @return the prev node 
	 */
	public TripStopNode getPrev() {
		return prev;
	}

	/**
	 * @param prev the prev to set
	 */
	public void setPrev(TripStopNode prev) {
		this.prev = prev;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return data.toString();
	}
	
	
	
}
