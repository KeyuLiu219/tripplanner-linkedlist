/**   
 * @Title: Itinerary.java 
 * @Package model 
 * @Description: TODO
 * @author pop
 * @date 2016年9月26日 下午9:37:57 
 * @version V1.0   
 */
package model;

import exception.EndOfItineraryException;

/**
 *
 * @Description: model the itinerary
 * @author panbo
 *
 */
public class Itinerary implements Cloneable{
	
	private TripStopNode head;
	
	private TripStopNode tail;
	
	private TripStopNode cursor;
	
	private Integer count;//the number of stops;
	
	private Integer totalDistance;//the total distance of Itinerary
	
	/**
	 * initializes an empty itinerary 
	 */
	public Itinerary() {
		head=tail=cursor=null;
		count = 0;
		totalDistance = 0;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		Itinerary itinerary=null;
        try {
        	itinerary=(Itinerary)super.clone(); 
        }
        catch (CloneNotSupportedException e) {
            throw new InternalError(e.toString());
        }
        itinerary.head=null;  
        TripStopNode newPrev=null;
        for(TripStopNode node=head;node!=null;node=node.getNext()){
        	TripStopNode temp=(TripStopNode)node.clone();
        	if(node==head) itinerary.head=temp;
        	if(node.getNext()==null) itinerary.tail=temp;
        	if(node==cursor) itinerary.cursor=temp;
        	temp.setPrev(newPrev);
        	if(newPrev!=null)  newPrev.setNext(temp);
        	newPrev=temp;        	
        }
        return itinerary;
	}
	
	/**
	 * @return the total number of TripStopNodes in the Itinerary.
	 */
	public int getStopsCount() {
		return this.count;
	}
	
	/**
	 * @return the sum of distances over all TripStopNodes.
	 */
	public int getTotalDistance() {
		return totalDistance;
	}
	
	/**
	 * @return the corsor trip stop
	 */
	public TripStop getCursorStop() {
		return cursor.getData();	
	}
	
	/**
	 * Reset the cursor to head node
	 */
	public void resetCursorToHead() {
		this.cursor=head;
	}

	/**
	 * Reset the cursor to tail node
	 */
	public void resetCursorToTail() {
		this.cursor=tail;
	}
	
	/**
	 * Moves the cursor to select the next TripStopNode in this list. If cursor == tail, throw an
	 * exception.
	 * @throws EndOfItineraryException
	 */
	public void cursorForward() throws EndOfItineraryException{
		if(cursor==tail) {
			throw new EndOfItineraryException("The cursor has moved to the tail of list");
		}
		cursor=cursor.getNext();
	}
	
	/**
	 * Moves the cursor to select the previous TripStopNode in this list. If cursor == head, throw an
	 * exception
	 * @throws EndOfItineraryException
	 */
	public void cursorBackward() throws EndOfItineraryException {
		if(cursor==head) {
			throw new EndOfItineraryException("The cursor has moved to the head of list");
		}
		cursor=cursor.getPrev();
	}
	
	/**
	 * Inserts the indicated TripStop before the cursor.
	 * @param newStop
	 * @throws IllegalArgumentException
	 */
	public void insertBeforeCursor(TripStop newStop) throws IllegalArgumentException{
		TripStopNode node = new TripStopNode(newStop);
		count++;
		totalDistance+=newStop.getDistance();
		if(cursor==null){
			head=tail=cursor=node;
			return;
		}else if(cursor==head){
			head=node;
		}else {
			cursor.getPrev().setNext(node);
		}
		node.setNext(cursor);
		node.setPrev(cursor.getPrev());
		cursor.setPrev(node);
		
		cursor=node;	
	}
	
	/**
	 * Inserts the indicated TripStop after the tail of the list.
	 * @param newStop
	 * @throws IllegalArgumentException
	 */
	public void appendToTail(TripStop newStop) throws IllegalArgumentException{
		TripStopNode node = new TripStopNode(newStop);
		count++;
		totalDistance+=newStop.getDistance();
		if(tail==null) {
			head=tail=node;
		}else {
			tail.setNext(node);
			node.setPrev(tail);
			tail=node;
		}
		cursor=node;
	}
	
	/**
	 * Removes the TripStopNode referenced by cursor and returns the TripStop inside
	 * @return 
	 * @throws EndOfItineraryException
	 */
	public TripStop removeCursor() throws EndOfItineraryException{
		if(cursor==null){
			throw new EndOfItineraryException("cursor is null");
		}
		TripStop rtnStop=cursor.getData();
		if(cursor.getPrev()!=null){
			cursor.getPrev().setNext(cursor.getNext());
		}else {
			head=head.getNext();
		}
		if(cursor.getNext()!=null){
			cursor.getNext().setPrev(cursor.getPrev());
		}
		cursor=cursor.getPrev()==null?head:cursor.getPrev();
		count--;
		totalDistance-=rtnStop.getDistance();
		return rtnStop;
	}

	/**
	 * @param stop the stop to edit
	 */
	public void editCursor(TripStop stop) {
		TripStop data = cursor.getData();
		if(!"".equals(stop.getActivity())){
			data.setActivity(stop.getActivity());
		}
		if(stop.getDistance()>=0){
			data.setDistance(stop.getDistance());
		}
		if(!"".equals(stop.getLocation())){
			data.setLocation(stop.getLocation());
		}
	}
	
	/**
	 * print all stops info
	 */
	public void printInfo() {
		if(count==0){
			System.out.println("\nThis trip has no stops.\n");
			return ;
		}
		int countBefore=0;
		int countAfter=0;
		boolean hasTraverseToCursor=false;
		for(TripStopNode node=head;node!=null;node=node.getNext()){
			if(!hasTraverseToCursor) countBefore+=1;
			else countAfter+=1;
			if(node==cursor){
				System.out.println(">"+node);
				hasTraverseToCursor=true;
				countBefore-=1;
			}
			else System.out.println(" "+node);
			
		}		
		System.out.println(String.format("\nSummary: This trip has %d stops, totaling %d miles. There are %d stops before the cursor and %d stops after the cursor.",count,totalDistance,countBefore,countAfter));
	}

	
	/**
	 * claer the itinerary
	 */
	public void clear() {
		for(TripStopNode node=head;node!=null;node=node.getNext()) {
			node.setData(null);
			node.setPrev(null);
		}
		head=tail=cursor=null;
	}


}
