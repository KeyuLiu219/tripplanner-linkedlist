package exception;


/**
 *
 * @Description:  Thrown to indicate that the cursor has move to the end of Itinerary
 * @author panbo
 *
 */
public class EndOfItineraryException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5057556856744658721L;
	
	public EndOfItineraryException() {
		super();
	}
	
	public EndOfItineraryException(String msg) {
		super(msg);
	}

}
