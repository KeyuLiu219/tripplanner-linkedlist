import java.util.Scanner;

import model.Itinerary;
import model.TripStop;

public class TripPlanner {

	private Itinerary firstItinerary;

	private Itinerary secondItinerary;

	private Itinerary currentItinerary;

	public TripPlanner() {
		// TODO Auto-generated constructor stub
	}

	public void init() {
		firstItinerary = currentItinerary = new Itinerary();
		secondItinerary = new Itinerary();

		printWelcome();
		printMenu();
	}

	private void printWelcome() {
		System.out.println("Welcome to TripPlanner!");
	}

	private void printMenu() {
		StringBuilder builder = new StringBuilder();
		builder.append("\nMenu:\n");
		builder.append("F‐Cursor forward\n");
		builder.append("B‐Cursor backward\n");
		builder.append("I‐Insert before cursor\n");
		builder.append("A‐Append to tail\n");
		builder.append("D‐Delete and move cursor forward\n");
		builder.append("H‐Cursor to Head\n");
		builder.append("T‐Cursor to Tail\n");
		builder.append("E‐Edit cursor\n");
		builder.append("S‐Switch itinerary\n");
		builder.append("O‐Insert cursor from other itinerary after cursor from this itinerary\n");
		builder.append("R‐Replace this itinerary with a copy of the other itinerary\n");
		builder.append("P‐Print current itinerary, including summary\n");
		builder.append("C‐Clear current itinerary\n");
		builder.append("Q‐Quit\n");

		System.out.println(builder.toString());
	}

	public void run() {
		Scanner scanner = new Scanner(System.in);
		while (handleInput(scanner)) {
			//printMenu();
		}
	}

	private boolean handleInput(Scanner scanner) {
		boolean ret=true;
		try{
		//read the instruction
		String input=scanner.nextLine().toUpperCase();
		if(input.equals("F")){
			currentItinerary.cursorForward();
			System.out.println("\nCursor forward.\n");
		}else if(input.equals("B"))
		{
			currentItinerary.cursorBackward();
			System.out.println("\nCursor moved back.\n");
		}else if(input.equals("I"))
		{
			TripStop stop=readTripStop(scanner,null);
			currentItinerary.insertBeforeCursor(stop);
			System.out.println("\nAdded.\n");
		}else if(input.equals("A"))
		{
			TripStop stop=readTripStop(scanner,null);
			currentItinerary.appendToTail(stop);
			System.out.println("\nAdded.\n");
		}else if(input.equals("D"))
		{
			currentItinerary.removeCursor();
			System.out.println("\ndeleted cursor.\n");
		}else if(input.equals("H"))
		{
			currentItinerary.resetCursorToHead();
			System.out.println("\nReset cursor to head.\n");
		}else if(input.equals("T"))
		{
			currentItinerary.resetCursorToTail();
			System.out.println("\nReset cursor to tail.\n");
		}else if(input.equals("E"))
		{
			TripStop stop=readEditTripStop(scanner,null);
			currentItinerary.editCursor(stop);
			System.out.println("\nEdits applied.\n");
		}else if(input.equals("S"))
		{
			currentItinerary=(currentItinerary==firstItinerary)?secondItinerary:firstItinerary;
			System.out.println("\nItinerary switched.\n");
		}else if(input.equals("O"))
		{
			if(currentItinerary==firstItinerary){
				TripStop stop=(TripStop)secondItinerary.getCursorStop().clone();
				if(stop!=null){
					currentItinerary.insertBeforeCursor(stop);
				}
			} else {
				TripStop stop=(TripStop)firstItinerary.getCursorStop().clone();
				if(stop!=null){
					currentItinerary.insertBeforeCursor(stop);
				}
			}
			System.out.println("\nInsert completed.\n");
		}else if(input.equals("R"))
		{
			if(currentItinerary==firstItinerary){
				currentItinerary=firstItinerary=(Itinerary)secondItinerary.clone();
			}
			else {
				currentItinerary=secondItinerary=(Itinerary)firstItinerary.clone();
			}
			System.out.println("\nItinerary copied.\n");
		}else if(input.equals("P"))
		{
			currentItinerary.printInfo();
		}else if(input.equals("C"))
		{
			currentItinerary.clear();
			System.out.println("\nClear completed.\n");
		}else if(input.equals("Q"))
		{
			ret=false;
			System.out.println("\nQuitting.\n");
		}else if(input.equals(""))
		{
			
		}
		else {
			throw new IllegalArgumentException("\nError input.Try again!\n");
		}
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return ret;
	}

	private TripStop readEditTripStop(Scanner scanner , TripStop stop) {
		if (stop == null) {
			stop = new TripStop();
		}
		try {
			if (stop.getLocation() == null) {
				System.out.print("\nEdit Location, or press enter without typing anything to keep:\n");
				stop.setLocation(scanner.nextLine());
			}
			if (stop.getActivity() == null) {
				System.out.print("\nEdit Activity, or press enter without typing anything to keep:\n");
				stop.setActivity(scanner.nextLine());
			}
			if (stop.getDistance() == null) {
				System.out.print("\nEdit Distance, or press enter without typing anything to keep:\n");
				stop.setDistance(scanner.nextInt());
			}
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
			readEditTripStop(scanner, stop);
		}
		return stop;

	}

	private TripStop readTripStop(Scanner scanner, TripStop stop) {
		if (stop == null) {
			stop = new TripStop();
		}
		try {
			if (stop.getLocation() == null) {
				System.out.print("Enter Location: ");
				stop.setLocation(scanner.nextLine());
			}
			if (stop.getActivity() == null) {
				System.out.print("Enter Activity: ");
				stop.setActivity(scanner.nextLine());
			}
			if (stop.getDistance() == null) {
				System.out.print("Enter Distance: ");
				stop.setDistance(scanner.nextInt());
			}
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
			readTripStop(scanner, stop);
		}
		return stop;
	}

	public static void main(String[] args) {
		TripPlanner planner = new TripPlanner();
		planner.init();
		planner.run();
		// Test();
	}
}
